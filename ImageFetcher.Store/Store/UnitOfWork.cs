﻿using MongoDB.Driver;

namespace ImageFetcher.Store
{
    public class UnitOfWork
    {
        private readonly IMongoDatabase _db;

        public IMongoCollection<dynamic> Urls => _db.GetCollection<dynamic>("Urls");
        public IMongoCollection<dynamic> Images => _db.GetCollection<dynamic>("Images");

        public UnitOfWork(string connectionString, string dbName)
        {
            var client = new MongoClient(connectionString);
            _db = client.GetDatabase(dbName);
        }

        //TODO: Add query and command classes
    }
}
