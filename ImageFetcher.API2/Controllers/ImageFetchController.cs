﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using ImageFetcher.API.Services;
using Microsoft.AspNetCore.Mvc;

namespace ImageFetcher.API2.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    public class ImageFetchController : ControllerBase
    {
        private readonly IImageFetchService _imageFetchService;

        public ImageFetchController(IImageFetchService imageFetchService)
        {
            _imageFetchService = imageFetchService;
        }

        // GET api/values/5
        [HttpGet("{id}")]
        public ActionResult<string> Get(int id)
        {
            return "value";
        }

        // POST api/values
        [HttpPost]
        public string[] FetchImages([FromBody] string url)
        {
            return _imageFetchService.FetchImages(url);
        }

    }
}
