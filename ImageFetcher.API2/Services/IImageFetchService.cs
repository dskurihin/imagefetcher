﻿namespace ImageFetcher.API.Services
{
    public interface IImageFetchService
    {
        string[] FetchImages(string url);
    }
}
