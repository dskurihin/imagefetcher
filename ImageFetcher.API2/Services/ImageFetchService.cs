﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using ImageFetcher.Core;
using ImageFetcher.Core.Helpers;
using ImageFetcher.Store;
using MongoDB.Driver;

namespace ImageFetcher.API.Services
{
    public class ImageFetchService : IImageFetchService
    {
        private readonly IImageFetcher _imageFetcher;
        private readonly IDBConfiguration _configuration;
        public ImageFetchService(IImageFetcher imageFetcher, IDBConfiguration configuration)
        {
            _imageFetcher = imageFetcher;
            _configuration = configuration;
        }

        public string[] FetchImages(string url)
        {
            _imageFetcher.Start(new Uri(url));
            bool urlProcessed = false;

            var uow = new UnitOfWork(_configuration.DBConnectionString, _configuration.DBName);
            while (!urlProcessed)
            {
                var urlData = uow.Urls.Find(Builders<dynamic>.Filter.Eq("url", url)).First();
                if (urlData == null)
                {
                    continue;
                }

                urlProcessed = urlData.processed;
            }

            var images = uow.Images.Find(Builders<dynamic>.Filter.Eq("url", url)).ToList();
                return  images.Select(x => x.filePath.ToString() as string).ToArray();
        }
    }
}
