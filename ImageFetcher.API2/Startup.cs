﻿using ImageFetcher.API.Services;
using ImageFetcher.Core;
using ImageFetcher.Core.Helpers;
using Microsoft.AspNetCore.Builder;
using Microsoft.AspNetCore.Hosting;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.DependencyInjection;

namespace ImageFetcher.API2
{
    public class Startup
    {
        public Startup(IConfiguration configuration)
        {
            Configuration = configuration;
        }

        public IConfiguration Configuration { get; }

        // This method gets called by the runtime. Use this method to add services to the container.
        public void ConfigureServices(IServiceCollection services)
        {
            services.AddMvc().SetCompatibilityVersion(CompatibilityVersion.Version_2_2);
            services.AddSingleton<IImageFetcher, Core.ImageFetcher>();
            services.AddSingleton<IValidationRules, ValidationRules>();
            services.AddSingleton<IFetcherProcessor, FetcherProcessor>();
            services.AddScoped<IDBConfiguration>(x => new DBConfiguration
            {
                DBName = Configuration.GetValue<string>("Database"),
                DBConnectionString = Configuration.GetValue<string>("ConnectionString")
            });
            services.AddSingleton<IImageFetchService, ImageFetchService>();
        }

        // This method gets called by the runtime. Use this method to configure the HTTP request pipeline.
        public void Configure(IApplicationBuilder app, IHostingEnvironment env)
        {
            if (env.IsDevelopment())
            {
                app.UseDeveloperExceptionPage();
            }
            else
            {
                // The default HSTS value is 30 days. You may want to change this for production scenarios, see https://aka.ms/aspnetcore-hsts.
                app.UseHsts();
            }

            app.UseHttpsRedirection();
            app.UseMvc();
        }
    }
}
