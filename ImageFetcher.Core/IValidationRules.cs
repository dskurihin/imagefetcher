﻿using System;
using HtmlAgilityPack;

namespace ImageFetcher.Core
{
    public interface IValidationRules
    {
        bool IsValidUri(Uri uri);
        bool IsValidPage(HtmlDocument document);
    }
}
