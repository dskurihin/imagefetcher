﻿namespace ImageFetcher.Core.Helpers
{
    public interface IDBConfiguration
    {
        string DBConnectionString { get; }
        string DBName { get; }
    }
}
