﻿namespace ImageFetcher.Core.Helpers
{
    public class DBConfiguration : IDBConfiguration
    {
        public string DBConnectionString { get; set; }
        public string DBName { get; set; }
    }
}
