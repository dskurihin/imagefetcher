﻿using System;

namespace ImageFetcher.Core
{
    public interface IImageFetcher
    {
        void Start(Uri uri);
    }
}
