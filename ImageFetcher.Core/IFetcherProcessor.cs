﻿using System;
using HtmlAgilityPack;

namespace ImageFetcher.Core
{
    public interface IFetcherProcessor
    {
        void Process(Uri uri, HtmlDocument document);
    }
}
