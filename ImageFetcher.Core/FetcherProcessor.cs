﻿using HtmlAgilityPack;
using System;
using System.Drawing;
using System.IO;
using System.Net;
using System.Runtime.InteropServices;
using ImageFetcher.Core.Helpers;
using ImageFetcher.Store;
using MongoDB.Driver;

namespace ImageFetcher.Core
{
    public class FetcherProcessor : IFetcherProcessor
    {
        private readonly string _imageFolder;
        private readonly string _dbConnectionString;
        private readonly string _dbName;
        private UnitOfWork _uow;

        public FetcherProcessor(string imageFolder, IDBConfiguration dbConfig)
        {
            _imageFolder = imageFolder;
            _dbConnectionString = dbConfig.DBConnectionString;
            _dbName = dbConfig.DBName;
            _uow = new UnitOfWork(_dbConnectionString, _dbName);
        }

        public void Process(Uri uri, HtmlDocument document)
        {
            if (uri == null || document == null)
                return;

            var images = document.DocumentNode.SelectNodes("//img[@src]");

            var folder = Path.Combine(_imageFolder, uri.Host);
            folder = Path.Combine(folder, uri.LocalPath.Substring(1).Replace("/", @"\"));

            if (images != null)
            {
                foreach (var image in images)
                {
                    try
                    {
                        var path = new Uri(uri, image.Attributes["@src"].Value);

                        var filePath = DownloadImage(path, folder);

                        SaveImagePathToDB(uri, filePath);

                        //TODO: Add logging
                    }
                    catch (UriFormatException)
                    {
                        //TODO: Add logging
                    }
                }
            }

            _uow.Urls.UpdateOne(Builders<dynamic>.Filter.Eq("url", uri.AbsolutePath), 
                Builders<dynamic>.Update.Set("processed", true));
        }

        private void SaveImagePathToDB(Uri uri, string filePath)
        {
            var builder = Builders<dynamic>.Filter;
            var filter = builder.Eq("filePath", filePath) & builder.Eq("url", uri.AbsolutePath);
            if (_uow.Images.Find(filter).CountDocuments() == 0)
                _uow.Images.InsertOne(new {url = uri.AbsolutePath, filePath = filePath});
        }

        private string DownloadImage(Uri url, string destination)
        {
            try
            {
                using (var client = new WebClient())
                {
                    var data = client.DownloadData(url);

                    using (var stream = new MemoryStream(data))
                    using (var image = Image.FromStream(stream))
                    {
                        var fileName = Path.GetFileName(url.AbsoluteUri);
                        if (fileName == null)
                            return null;

                        if (!Directory.Exists(destination))
                            Directory.CreateDirectory(destination);

                        var fullPath = Path.Combine(destination, fileName);
                        if (!File.Exists(fullPath))
                            image.Save(fullPath);
                        return fullPath;
                    }
                }
            }
            catch (WebException)
            {
                //TODO: Add logging
                return null;
            }
            catch (ArgumentException)
            {
                //TODO: Add logging
                return null;
            }
            catch (ExternalException)
            {
                //TODO: Add logging
                return null;
            }
        }

      
    }
}
