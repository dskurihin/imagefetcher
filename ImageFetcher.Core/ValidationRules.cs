﻿using System;
using System.Collections.Concurrent;
using System.Linq;
using HtmlAgilityPack;

namespace ImageFetcher.Core
{
    public class ValidationRules : IValidationRules
    {
        private readonly ConcurrentBag<string> _crawled = new ConcurrentBag<string>();

        private readonly string _scope;

        public ValidationRules(string scope)
        {
            _scope = scope;
        }

        public bool IsValidUri(Uri uri)
        {
            if (uri == null)
                return false;

            if (!string.IsNullOrWhiteSpace(uri.Fragment))
                return false;

            if (_crawled.Contains(uri.AbsoluteUri))
                return false;

            if (!uri.AbsoluteUri.StartsWith(_scope, StringComparison.OrdinalIgnoreCase))
                return false;

            _crawled.Add(uri.AbsoluteUri);

            return true;
        }

        public bool IsValidPage(HtmlDocument document)
        {
            //TODO: Add some logic here
            return true;
        }
    }
}
