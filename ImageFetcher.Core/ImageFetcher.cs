﻿using System;
using System.Collections.Concurrent;
using System.Collections.Generic;
using System.Net;
using System.Threading;
using System.Threading.Tasks;
using HtmlAgilityPack;
using ImageFetcher.Core.Helpers;
using ImageFetcher.Store;

namespace ImageFetcher.Core
{
    public class ImageFetcher : IImageFetcher
    {
        private readonly List<Uri> _urls = new List<Uri>();
        private readonly IValidationRules _rules;
        private readonly IFetcherProcessor _processor;
        private readonly HtmlWeb _web = new HtmlWeb();
        private readonly string _dbConnectionString;
        private readonly string _dbName;

        public ImageFetcher(IValidationRules rules, IFetcherProcessor processor, int threads, IDBConfiguration dbConfig)
        {
            _rules = rules;
            _processor = processor;
            _dbConnectionString = dbConfig.DBConnectionString;
            _dbName = dbConfig.DBName;

            Task.Factory.StartNew(() => Crawl(threads), TaskCreationOptions.LongRunning);
        }

        public void Start(Uri uri)
        {
            Add(uri);
        }

        private void Add(Uri uri)
        {
            if (!_rules.IsValidUri(uri))
                return;
            _urls.Add(uri);
            var uow = new UnitOfWork(_dbConnectionString, _dbName);
            uow.Urls.InsertOne(new {url= uri.AbsolutePath, processed = false });
        }

        private void Crawl(int threads)
        {
            var src = Partitioner.Create(_urls, EnumerablePartitionerOptions.NoBuffering);

            Parallel.ForEach(src, new ParallelOptions { MaxDegreeOfParallelism = threads }, Crawl);
        }

        private void Crawl(Uri uri)
        {
            HtmlDocument doc = null;
            try
            {
                doc = _web.Load(uri.AbsoluteUri);
            }
            catch (ArgumentException ex)
            {
               //TODO: Add Logging
            }
            catch (WebException ex)
            {
                //TODO: Add Logging
            }

            if (doc == null)
                return;

            if (!_rules.IsValidPage(doc))
                return;

            _processor.Process(uri, doc);

            var images = doc.DocumentNode.SelectNodes("//img");

            if (images == null)
                return;

            foreach (var image in images)
            {
                try
                {
                    var path = new Uri(uri, image.Attributes["@src"].Value);

                    Add(path);
                }
                catch (UriFormatException ex)
                {
                    //TODO: Add Logging
                }
            }
        }
    }
}
